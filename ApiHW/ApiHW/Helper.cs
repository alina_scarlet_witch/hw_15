﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;
using System.IO;

namespace ApiHW
{
    public class Helper
    {
        public string id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string userId { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string albumId { get; set; }
        public string url { get; set; }
        public string thumbnailUrl { get; set; }
        public string completed { get; set; }
    }
}
