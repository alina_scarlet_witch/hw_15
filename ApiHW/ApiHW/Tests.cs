﻿using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;

namespace ApiHW
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public static void CreatePostInPosts()
        {
            string jsonString = @"{
                ""title"": ""board games"",
                ""body"": ""sudak"",
                ""userId"": ""1""
              }";
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts");
            var restRequest = restApi.CreatePostRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Helper content = restApi.GetContent<Helper>(response);
            Assert.AreEqual(content.title, "board games");
            Assert.AreEqual(content.body, "sudak");
            Assert.AreEqual(content.userId, "1");
        }

        [Test]
        public static void UpdatingAResource()
        {
            string jsonString = @"{
                ""title"": ""Maybe"",
                ""body"": ""sudak"",
                ""userId"": ""1""
              }";
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts/1");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Helper content = restApi.GetContent<Helper>(response);
            Assert.AreEqual(content.title, "Maybe");
            Assert.AreEqual(content.body, "sudak");
            Assert.AreEqual(content.userId, "1");
        }

        [Test]
        public static void GettingAResource()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts/1");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Helper content = restApi.GetContent<Helper>(response);
            Assert.AreEqual(content.title, "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
            Assert.AreEqual(content.body, "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto");
            Assert.AreEqual(content.userId, "1");
        }

        [Test]
        public static void PatchingAResource()
        {
            string jsonString = @"{
               ""title"": ""Maybe"",
                ""body"": ""sudak"",
                ""userId"": ""1""
              }";
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts/1");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Helper content = restApi.GetContent<Helper>(response);
            Assert.AreEqual(content.title, "Maybe");
            Assert.AreEqual(content.body, "sudak");
            Assert.AreEqual(content.userId, "1");

        }

        [Test]
        public static void DeletingAResource()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts/1");
            var restRequest = restApi.CreateDeleteRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Helper content = restApi.GetContent<Helper>(response);
            Assert.AreEqual(content.title, null);
            Assert.AreEqual(content.body, null);
            Assert.AreEqual(content.userId, null);
        }

        [Test]
        public static void FilteringResources()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts?Id=1");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            Assert.AreEqual(content[0].title, "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
            Assert.AreEqual(content[0].body, "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto");
            Assert.AreEqual(content[0].userId, "1");
        }

        [Test]
        public static void ListingAllResources()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            int number = content.Count;
            Assert.AreEqual(number, 100);
        }

        [Test]
        public static void ListingNestedResources()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("posts/1/comments");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            int number = content.Count;
            Assert.AreEqual(number, 5);
            Assert.AreEqual(content[4].name, "vero eaque aliquid doloribus et culpa");
            Assert.AreEqual(content[4].body, "harum non quasi et ratione\ntempore iure ex voluptates in ratione\nharum architecto fugit inventore cupiditate\nvoluptates magni quo et");
        }

        [Test]
        public static void PhotosInAlbums()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("albums/1/photos");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            int number = content.Count;
            Assert.AreEqual(number, 50);
            Assert.AreEqual(content[49].id, "50");
            Assert.AreEqual(content[49].title, "et inventore quae ut tempore eius voluptatum");
            Assert.AreEqual(content[49].url, "https://via.placeholder.com/600/9e59da");
            Assert.AreEqual(content[49].thumbnailUrl, "https://via.placeholder.com/150/9e59da");
        }

        [Test]
        public static void UsersInAlbums()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("users/1/albums");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            int number = content.Count;
            Assert.AreEqual(number, 10);
            Assert.AreEqual(content[9].id, "10");
            Assert.AreEqual(content[9].title, "distinctio laborum qui");
        }

        [Test]
        public static void TodosInAlbums()
        {
            ApiRequest<Helper> restApi = new ApiRequest<Helper>();
            var restUrl = restApi.SetUrl("users/1/todos");
            var restRequest = restApi.CreateGetRequest();
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            List<Helper> content = restApi.GetListWithContents<Helper>(response);
            int number = content.Count;
            Assert.AreEqual(number, 20);
            Assert.AreEqual(content[19].id, "20");
            Assert.AreEqual(content[19].title, "ullam nobis libero sapiente ad optio sint");
            Assert.AreEqual(content[19].completed, "true");
        }
    }
}







